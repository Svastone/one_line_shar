require("dotenv").config();
const HDWalletProvider = require("truffle-hdwallet-provider");
const { MNEMONIC, ETHERSCAN_API_KEY, PROJECT_ID } = process.env;
// ... rest of truffle-config
module.exports = {
plugins: ["truffle-plugin-verify"],
  networks: {
    dashboard: {
      port: 8575,
      host: "127.0.0.1",
      verbose: false,
     },

    linea: {
      provider: function() {
        return new HDWalletProvider(
          MNEMONIC, PROJECT_ID,
       );
      },
      verify: {
        apiUrl: "https://explorer.goerli.linea.build/api",
        apiKey: ETHERSCAN_API_KEY,
        explorerUrl: "https://explorer.goerli.linea.build/",
      },
        poolingInterval: 30000,
        networkCheckTimeoutnetworkCheckTimeout: 10000,
        timeoutBlocks: 50,
        network_id: "59140",
    },
  },
   compilers: {
        solc: {
            version: "0.8.19", // Fetch exact version from solc-bin (default: truffle's version)
            //       docker: true,        // Use "0.5.1" you've installed locally with docker (default: false)
            settings: {
                // See the solidity docs for advice about optimization and evmVersion
                optimizer: {
                    enabled: true,
                    runs: 200
                },
                //        evmVersion: "byzantium"
            },
        },
    },
};

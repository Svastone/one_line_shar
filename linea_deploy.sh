#!/bin/bash


sudo apt update; apt upgrade

mkdir linea

cd linea

if dpkg -s | grep -q nodejs ; then
                 echo NodeJS" Installed"
        else
                 echo NodeJS" Not installed" && sudo apt install nodejs
        fi

if dpkg -s | grep -q npm ; then
                 echo Npm" Installed"
        else
                 echo Npm" Not installed" && sudo npm i -g npm@latest
        fi

if npm list -g truffle | grep -q truffle ; then
                 echo Truffle" Installed"
        else
                 echo Truffle" Not installed" && npm i -g truffle
        fi
if npm list -g solc | grep -q solc ; then
                 echo Solidity" Installed"
        else
                 echo Solidity" Not installed" && npm i -g solc
        fi
if npm list -g dotenv | grep -q dotenv ; then
                 echo DotEnv" Installed"
        else
                 echo DotEnv" Not installed" && npm i dotenv --save
        fi

if npm list -g @truffle/hdwallet-provider | grep -q @truffle/hdwallet-provider ; then
                 echo HDWallet" Installed"
        else
                 echo HDWallet" Not installed" && npm i @truffle/hdwallet-provider --save
        fi
        

sudo apt-get install -y gconf-service libasound2 libatk1.0-0 libatk-bridge2.0-0 libc6 libcairo2 libcups2 libdbus-1-3 libexpat1 libfontconfig1 libgcc1 libgconf-2-4 libgdk-pixbuf2.0-0 libglib2.0-0 libgtk-3-0 libnspr4 libpango-1.0-0 libpangocairo-1.0-0 libstdc++6 libx11-6 libx11-xcb1 libxcb1 libxcomposite1 libxcursor1 libxdamage1 libxext6 libxfixes3 libxi6 libxrandr2 libxrender1 libxss1 libxtst6 ca-certificates fonts-liberation libappindicator1 libnss3 lsb-release xdg-utils wget && sudo apt-get install -y libgbm-dev

truffle init

rm -f $HOME/contracts/Token.io

rm -f $HOME/linea/truffle-config.js

wget -P $HOME/linea/contracts https://gitlab.com/Svastone/one_line_shar/-/raw/ec6ff7af17f6d9f4163034f02667fa7937715b8f/Token.sol && truffle compile

wget -P $HOME/linea/migrations https://gitlab.com/Svastone/one_line_shar/-/raw/ec6ff7af17f6d9f4163034f02667fa7937715b8f/1_deploy_token.js

wget -P $HOME/linea https://gitlab.com/Svastone/one_line_shar/-/raw/main/truffle-config.js

wget -P $HOME/linea https://gitlab.com/Svastone/one_line_shar/-/raw/main/.env && nano $HOME/linea/.env



